from django.db import models
from django.conf import settings
from order.models import Order
# oid=239&amt=1680.0&refId=0008LEL


class eSewa(models.Model):
	order = models.ForeignKey(Order)

	refId = models.CharField(max_length=255)
	amount = models.DecimalField(max_digits=64, decimal_places=2, default=0, blank=True, null=True)
	date = models.DateTimeField(blank=True, null=True, help_text="HH:MM:SS DD Mmm YY, YYYY PST")
    